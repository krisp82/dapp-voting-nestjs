import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { VotingService } from './voting.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, VotingService],
  exports:[VotingService]
})
export class AppModule {}
