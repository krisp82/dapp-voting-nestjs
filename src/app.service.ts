import { Injectable } from '@nestjs/common';
import {VotingService} from './voting.service';
@Injectable()
export class AppService {
  constructor(private readonly votingService: VotingService){}
  getHello(): string {
    return 'Hello World!';
  }

  showCandidateScore(name: string): number
  {
    console.log("showCandidateScore: "+ name);
    let score = this.votingService.totalVotesForCandidate(name);
    console.log("score: "+ score);
    return score;
  }

  voteForCandidate(name: string): number
  {
    console.log("showCandidateScore: "+ name);
    let score = this.votingService.voteForCandidate(name);
    console.log("score: "+ score);
    return score;
  }
}
