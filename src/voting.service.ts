import { Injectable } from '@nestjs/common';
import {ethers} from 'ethers';

@Injectable()
export class VotingService {
    private contract;
    private readonly voting_abi: string = '[{"constant":true,"inputs":[{"name":"candidate","type":"bytes32"}],"name":"totalVotesFor","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"candidate","type":"bytes32"}],"name":"validCandidate","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"bytes32"}],"name":"votesReceived","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"candidateList","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"candidate","type":"bytes32"}],"name":"voteForCandidate","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"candidateNames","type":"bytes32[]"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"}]';
    private readonly voting_bin: string = '608060405234801561001057600080fd5b506040516103d93803806103d98339810180604052602081101561003357600080fd5b81019080805164010000000081111561004b57600080fd5b8281019050602081018481111561006157600080fd5b815185602082028301116401000000008211171561007e57600080fd5b5050929190505050806001908051906020019061009c9291906100a3565b5050610115565b8280548282559060005260206000209081019282156100df579160200282015b828111156100de5782518255916020019190600101906100c3565b5b5090506100ec91906100f0565b5090565b61011291905b8082111561010e5760008160009055506001016100f6565b5090565b90565b6102b5806101246000396000f3fe608060405234801561001057600080fd5b50600436106100575760003560e01c80632f265cf71461005c578063392e66781461009e5780637021939f146100e4578063b13c744b14610126578063cc9ab26714610168575b600080fd5b6100886004803603602081101561007257600080fd5b8101908080359060200190929190505050610196565b6040518082815260200191505060405180910390f35b6100ca600480360360208110156100b457600080fd5b81019080803590602001909291905050506101c4565b604051808215151515815260200191505060405180910390f35b610110600480360360208110156100fa57600080fd5b810190808035906020019092919050505061021a565b6040518082815260200191505060405180910390f35b6101526004803603602081101561013c57600080fd5b8101908080359060200190929190505050610232565b6040518082815260200191505060405180910390f35b6101946004803603602081101561017e57600080fd5b8101908080359060200190929190505050610253565b005b60006101a1826101c4565b6101aa57600080fd5b600080838152602001908152602001600020549050919050565b600080600090505b60018054905081101561020f5782600182815481106101e757fe5b90600052602060002001541415610202576001915050610215565b80806001019150506101cc565b50600090505b919050565b60006020528060005260406000206000915090505481565b6001818154811061023f57fe5b906000526020600020016000915090505481565b61025c816101c4565b61026557600080fd5b6001600080838152602001908152602001600020600082825401925050819055505056fea165627a7a723058203f1d8ce02d43e77d6efbbc784c0ff6aba2b242d36f3ed76251afef66c8b7f3070029';
    constructor()
    {
        const fs = require('fs');
        // const abi = JSON.parse(fs.readFileSync('Voting_sol_WishVoting.abi').toString());
        // use ethers
        const provider = new ethers.providers.JsonRpcProvider();
        const signer = provider.getSigner(0);
        const abi = JSON.parse(this.voting_abi);
        // this.contract = new ethers.Contract(this.voting_bin, abi, signer);
        const factory = new ethers.ContractFactory(abi, this.voting_bin, signer);
        const listCandidate = ['Alex','Nueng', 'View','Wish','Chaow','Simon','Flash','Mateuos'];
        const candidates = listCandidate.map( (name) => ethers.utils.formatBytes32String(name));
        console.log(candidates);
        factory.deploy(
            candidates
        )
            // [ethers.utils.formatBytes32String('Alex'), ethers.utils.formatBytes32String('Chaow'), ethers.utils.formatBytes32String('Wish')])
        .then( c => { 
         this.contract = c;
        });
        
    }
  totalVotesForCandidate(name: string): number {
    return this.contract.totalVotesFor(ethers.utils.formatBytes32String(name))
    .then( (f) =>  f.toNumber())
    .catch((err) => {
        console.log(err);
        return -1;
    })
  }

  voteForCandidate(name: string): number
  {
    return this.contract.voteForCandidate(ethers.utils.formatBytes32String(name))
    .then((f) => {
       return (this.contract.totalVotesFor(ethers.utils.formatBytes32String(name))
        .then((f) => f.toNumber())
        .catch( (err) => {
            console.log(err);
            return -1;
        }));
    }).catch( (err) => {
        console.log(err);
        return -1;
    }) ;
  }
}
