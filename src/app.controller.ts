import { Controller, Get, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/candidate/:name')
  getCandidate(@Param('name') name: string) : number{
    console.log("/candidate/"+name);
    return this.appService.showCandidateScore(name);
  }

  @Get('/candidate/vote/:name')
  voteCandidate(@Param('name') name: string): number
  { 
    console.log("/candidate/vote/"+name);
    return this.appService.voteForCandidate(name);
  }
}
